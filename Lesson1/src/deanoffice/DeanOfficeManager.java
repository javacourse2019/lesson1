
package deanoffice;


import java.util.ArrayList;
import java.util.List;
import students.Student;


public class DeanOfficeManager
{
    
    
    Student st;
    
    List<Student> studentDatabase = new ArrayList<>();

    public DeanOfficeManager()
    {
        
    }

  
    public void createStudent(String f, String i, String o, String g)
    {
        
        Student st = new Student(f,i,o,g);
        studentDatabase.add(st); 
        System.out.println("Добавлен студент {"+f+"}");
    }
    
    
    public int getDatabaseSize()
    {
        return studentDatabase.size();
    }        
            
    
    
}
