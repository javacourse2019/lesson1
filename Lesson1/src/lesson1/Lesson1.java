
package lesson1;

import deanoffice.DeanOfficeManager;
import students.Student;

/**
 *
 * @author vaganovdv
 */
public class Lesson1
{

    
    public static void main(String[] args)
    {
        DeanOfficeManager  dekanatZO = new DeanOfficeManager();
         DeanOfficeManager dekanatDO = new DeanOfficeManager();
        
        dekanatZO.createStudent("Путин", "Владимир", "Владимирович", "ПКС");
        int dbsize = dekanatZO.getDatabaseSize();
        System.out.println("Размер базы данных: "+dbsize);
        
        dekanatZO.createStudent("Жириновский", "Владмир", "Вольфович", "ПКС");
        dbsize = dekanatZO.getDatabaseSize();
        System.out.println("Размер базы данных: "+dbsize);
    }
    
}
