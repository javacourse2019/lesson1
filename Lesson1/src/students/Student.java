
package students;


public class Student
{
    
    long id;        // Номер стулента в БД
    String fam;     // Поле фамилии    
    String im;      // Поле имени
    String ot;      // Поле отчества
    String group;   // Поле группы
    String studyYear; // Год обучения

    public Student(String fam, String im, String ot, String group)
    {
        this.fam = fam;
        this.im = im;
        this.ot = ot;
        this.group = group;
    }

   
    
    public Student(String famil)
    {
        this.fam = famil;
        this.im = "";
        this.ot = "";
        
    }
    
     
    public Student()
    {
        this.fam = "";
        this.im = "";
        this.ot = "";
    }
    
}
